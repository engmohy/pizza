"""pizza URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from pizza.viewsets.customer_viewset import CustomersViewSet
from pizza.viewsets.orders_viewset import OrderStatusView, OrdersViewSet, OrderDetailsView
from pizza.viewsets.products_viewset import ProductsViewSet

router = DefaultRouter()
router.register(prefix='customer', viewset=CustomersViewSet, base_name='customer')
router.register(prefix='product', viewset=ProductsViewSet, base_name='product')
router.register(prefix='order', viewset=OrdersViewSet, base_name='order')
router.register(prefix='order_status', viewset=OrderStatusView)
router.register(prefix='order_details', viewset=OrderDetailsView)

urlpatterns = [
    url(r'^api/', include((router.urls, 'pizza'), namespace='api'), )
]
