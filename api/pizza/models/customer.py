from django.db import models


class Customers(models.Model):
    """
    Model for customer detail
    """
    telephone_no = models.CharField(max_length=100, unique=True)
    name_ar = models.CharField(max_length=100, blank=True)
    name_en = models.CharField(max_length=100, blank=True)
    mail = models.EmailField(blank=True)
    address = models.CharField(max_length=200, blank=True)
    create_at = models.DateField(auto_now=True)

    def __str__(self):
        return self.name_en
