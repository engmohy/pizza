from django.db import models


class Products(models.Model):
    name_ar = models.CharField(max_length=100)
    name_en = models.CharField(max_length=100)
    unit_price = models.IntegerField()
    details = models.TextField(null=True, blank=True)
    size = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name_en
