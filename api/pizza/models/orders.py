from django.db import models

from pizza.models.customer import Customers
from pizza.models.products import Products


class Orders(models.Model):
    customer_id = models.ForeignKey(Customers, on_delete=models.CASCADE)
    total_price = models.IntegerField(null=True, blank=True)
    order_comments = models.CharField(max_length=200, blank=True)
    create_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"Order number {self.id} : for client {self.customer_id.name_en}"


class OrderStatusDetails(models.Model):
    order_id = models.ForeignKey(Orders, on_delete=models.CASCADE)
    order_status = models.CharField(max_length=100, default='new', choices=(
        ('new', 'new'), ('in_progress', 'in_progress'), ('canceled', 'canceled'),
        ('done', 'done'), ('with-delivery-agent', 'with-delivery-agent')))
    status_datetime = models.DateTimeField(auto_now=True)
    comments = models.CharField(max_length=200, blank=True)

    # ToDo in case of the order can be undo from any state to previous
    #  one then comment the meta class and migrate
    class Meta:
        """
        this will prevent creating duplicated status for same order
        """
        unique_together = ('order_status', 'order_id',)


class OrderDetails(models.Model):
    order_id = models.ForeignKey(Orders, on_delete=models.CASCADE)
    product_id = models.ForeignKey(Products, on_delete=models.CASCADE)
    quantity = models.IntegerField()
