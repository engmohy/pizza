# -*- coding: utf-8 -*-
from django_filters import rest_framework
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework.viewsets import ModelViewSet

from pizza.models.products import Products
from pizza.serializers.products_serializer import ProductsSerializer


class Filter(rest_framework.FilterSet):
    class Meta:
        model = Products
        fields = ['name_en', 'name_ar', 'unit_price', 'details', 'size']


class ProductsViewSet(ModelViewSet):
    queryset = Products.objects.all()
    serializer_class = ProductsSerializer

    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    filter_class = Filter
    search_fields = ['name_en', 'name_ar', 'unit_price', 'details', 'size']
