# -*- coding: utf-8 -*-
from django_filters import rest_framework
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework.viewsets import ModelViewSet

from pizza.models.customer import Customers
from pizza.serializers.customer_serializer import CustomersSerializer


class Filter(rest_framework.FilterSet):
    class Meta:
        model = Customers
        fields = ['telephone_no', 'name_en', 'name_ar', 'mail', 'address', 'create_at']


class CustomersViewSet(ModelViewSet):
    queryset = Customers.objects.all()
    serializer_class = CustomersSerializer

    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    filter_class = Filter
    search_fields = ['telephone_no', 'name_en', 'name_ar', 'mail', 'address', 'create_at']
