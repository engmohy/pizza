# -*- coding: utf-8 -*-

from django_filters import rest_framework
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework.viewsets import ModelViewSet

from pizza.models.orders import Orders, OrderStatusDetails, OrderDetails
from pizza.serializers.orders_serializer import OrdersSerializer, OrderDetailsSerializer, OrderStatusDetailsSerializer


class FilterOrder(rest_framework.FilterSet):

    class Meta:
        model = Orders
        fields = {
            'create_at': ['lte', 'gte', 'lt', 'gt'],
            'customer_id': ['exact'],
        }


class OrdersViewSet(ModelViewSet):
    queryset = Orders.objects.all()
    serializer_class = OrdersSerializer

    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    filter_class = FilterOrder
    search_fields = [
        "create_at"
    ]


class FilterStatus(rest_framework.FilterSet):
    class Meta:
        model = OrderStatusDetails
        fields = {
            'status_datetime': ['lte', 'gte', 'lt', 'gt'],
            'comments': ['exact'],
            'order_status': ['exact'],
            'order_id': ['exact'],
        }


class OrderStatusView(ModelViewSet):
    queryset = OrderStatusDetails.objects.all()
    serializer_class = OrderStatusDetailsSerializer

    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    filter_class = FilterStatus
    search_fields = [
        'status_datetime',
        'comments',
        'order_status',
        'order_id',
    ]


class FilterDetails(rest_framework.FilterSet):
    class Meta:
        model = OrderDetails
        fields = {
            'product_id',
            'order_id'
        }


class OrderDetailsView(ModelViewSet):
    queryset = OrderDetails.objects.all()
    serializer_class = OrderDetailsSerializer

    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    filter_class = FilterDetails
    search_fields = [
        'product_id',
        'order_id',
    ]
