from rest_framework import serializers

from pizza.models.customer import Customers


class CustomersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customers
        fields = ("__all__")
