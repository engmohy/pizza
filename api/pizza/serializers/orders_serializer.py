# -*- coding: utf-8 -*-
from rest_framework import serializers

from pizza.models.orders import Orders, OrderDetails, OrderStatusDetails


class OrdersSerializer(serializers.ModelSerializer):
    customer_name = serializers.CharField(source='customer_id.name_en', read_only=True)
    customer_address = serializers.CharField(source='customer_id.address', read_only=True)
    customer_phone = serializers.CharField(source='customer_id.telephone_no', read_only=True)
    product_details = serializers.SerializerMethodField()
    total_price = serializers.SerializerMethodField()

    def validate(self, data):
        """
        Check that order status is not done.
        """
        if hasattr(self.instance, 'id'):
            order_status = OrderStatusDetails.objects.filter(order_id=self.instance.id).filter(
                order_status__in=['done', 'canceled']).first()
            if order_status:
                raise serializers.ValidationError("this order cannot be updated (done or canceled)")
        return data

    class Meta:
        model = Orders
        fields = ("__all__")

    def get_total_price(self, obj):
        order_details = OrderDetails.objects.filter(order_id=obj.pk).all()
        price = 0
        for o in order_details:
            price += o.quantity * o.product_id.unit_price
        return price

    def get_product_details(self, obj):
        order_details = OrderDetails.objects.filter(order_id=obj.pk).all()
        product_details = []
        for o in order_details:
            product_details.append({
                "name_en": o.product_id.name_en,
                "quantity": o.quantity,
                "unit_price": o.product_id.unit_price,
            })
        return product_details


class OrderStatusDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderStatusDetails
        fields = ("__all__")


class OrderDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderDetails
        fields = ("__all__")

    def validate(self, data):
        """
        Check that order status is not done.
        """
        order_status = OrderStatusDetails.objects.filter(order_id=data['order_id'].id).filter(
            order_status__in=['done', 'canceled']).first()
        if order_status:
            raise serializers.ValidationError("this order cannot be updated (done or canceled)")
        return data
