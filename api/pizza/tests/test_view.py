from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from pizza.models.customer import Customers
from pizza.models.products import Products


class PizzaTests(APITestCase):

    def test_create_customer(self):
        """
        Ensure we can create update delete a new customer.
        """
        url = reverse('api:customer-list')
        data = {
            'telephone_no': '010109992',
            'name_ar': 'محمد محي',
            'name_en': 'mohamed mohy',
            'address': 'cairo',
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Customers.objects.count(), 1)
        self.assertEqual(Customers.objects.get().name_en, 'mohamed mohy')
        response = self.client.patch(url + str(Customers.objects.get().id) + '/',
                                     {'telephone_no': '9999'}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Customers.objects.get().telephone_no, '9999')
        # response = self.client.delete(url + str(Customers.objects.get().id) + '/', {'telephone_no': '9999'},
        #                               format='json')
        # self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        # self.assertEqual(len(Customers.objects.all()), 0)

    def test_product(self):
        """
        Ensure we can create update delete a new product.
        """
        url = reverse('api:product-list')
        data = {
            'name_en': 'shawrma',
            'name_ar': 'شورما فراخ',
            'unit_price': 60,
            'size': 'small'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Products.objects.count(), 1)
        self.assertEqual(Products.objects.get().name_en, 'shawrma')
        response = self.client.patch(url + str(Products.objects.get().id) + '/',
                                     {'size': 'large'}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Products.objects.get().size, 'large')
        # response = self.client.delete(url + str(Customers.objects.get().id) + '/', {'telephone_no': '9999'},
        #                               format='json')
        # self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        # self.assertEqual(len(Customers.objects.all()), 0)
