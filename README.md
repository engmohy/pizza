## Pizza project

### Install and run via docker 
* `docker-compose up`
### Install and run without docker 
```bash
cd api
# optional create virtual env and activate
pip install -r requirements.txt
python manage.py migrate
python manage.py runserver

``` 

### API Endpoints

both customer and product and order are totally restful api 
####customer
```
| endpoint           	| method 	| desc             	|
|--------------------	|--------	|------------------	|
| /api/customer      	| post   	| add new customer 	|
| /api/customer/<id> 	| patch  	| partial update   	|
| /api/customer/<id> 	| put    	| full update      	|
| /api/customer/<id> 	| delete 	| delete customer  	|

```
####product
```
| endpoint           	| method 	| desc             	|
|--------------------	|--------	|------------------	|
| /api/product      	| post   	| add new product 	|
| /api/product/<id> 	| patch  	| partial update   	|
| /api/product/<id> 	| put    	| full update      	|
| /api/product/<id> 	| delete 	| delete customer  	|

```
####orders
- order have 3 table 
- the main order 
- the details for multi pizza in same order 
- order status for tracking the status with time and extra info

####CRD order 
```
| endpoint           	| method 	| desc             	|
|--------------------	|--------	|------------------	|
| /api/order            | post   	| add new order 	|
| /api/order/<id>       | patch  	| partial update   	|
| /api/order/<id>   	| put    	| full update      	|
| /api/order/<id>   	| delete 	| delete  cascade  	|

```
####CRD order details
```
| endpoint                	| method 	| desc             	|
|-------------------------	|--------	|------------------	|
| /api/order_details      	| post   	| add new customer 	|
| /api/order_details/<id> 	| patch  	| partial update   	|
| /api/order_details/<id> 	| put    	| full update      	|
| /api/order_details/<id> 	| delete 	| delete customer  	|

```
####CRD order status
```
| endpoint               	| method 	| desc             	|
|------------------------	|--------	|------------------	|
| /api/order_status      	| post   	| add new customer 	|
| /api/order_status/<id> 	| patch  	| partial update   	|
| /api/order_status/<id> 	| put    	| full update      	|
| /api/order_status/<id> 	| delete 	| delete customer  	|
```
#### filter order by customer_id or date greater than = less than
`api/order/?create_at__lte=&create_at__gte=&create_at__lt=&create_at__gt=&customer_id=`
#### get order details 
`api/order_details/?order_id=`
#### get order status order by date 
`api/order_status/?ordering=-status_datetime&order_id=<id>`


- toDo write script to force docker wait fo db init
